import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "./services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "app";
  isLogin = true;

  constructor(
    private api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private router: Router
  ) {}

  ngOnInit() {
    if (!this.cookies.get("orianaSellerToken")) {
      this.router.navigate(["/"]);
    }
  }
}
