import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ApiService } from "../../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";
@Component({
  selector: "app-login-confirm",
  templateUrl: "./login-confirm.component.html",
  styleUrls: ["./login-confirm.component.css"]
})
export class LoginConfirmComponent implements OnInit {
  public display: string = "none";
  public cModal: any = "";
  public femail: any;
  public lemail: any;
  public lpassword: any;
  public rname: any;
  public remail: any;
  public rpassword: any;
  public regMsg: any;
  constructor(
    private api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      var confirmCode = params["confirmCode"];
      this.confirmAccount(confirmCode);
    });
  }

  public openModal() {
    this.display = "block";
  }

  public onCloseHandled() {
    this.display = "none";
  }

  // Api to get forget password for Buyer

  public forgetPwdForm(formData) {
    if (formData) {
      let pattern: any = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      var data = formData["femail"];
      if (pattern.test(data)) {
        this.femail = formData["femail"];
        console.log(formData["femail"]);
        //this.spinner.show()
        this.api
          .post("/api/v1/seller/forget-password", { email: formData["femail"] })
          .subscribe(
            (res: Response) => {
              console.log("res");
              console.log(res);
              //this.spinner.hide();
              //this.display = "none";
              if (res["status"].toString() == "success") {
                this.toast.success(res["message"]);

                this.cModal = 1;
              } else {
                this.toast.error("Some Problem Occured");
              }
            },
            (err: Response) => {
              this.display = "none";
              console.log("err");
              console.log(err);
              //this.spinner.hide();
              this.toast.error("Something went wrong");

              if (err["error"].message == "Errors") {
                this.toast.error(err["error"].data.errors[0].msg);
              } else {
                this.toast.error(err["error"].message);
              }
            }
          );
      } else {
        this.toast.error("Enter valid email");
      }
    }
  }
  // Api to get confirmation code for forget password for Buyer

  public cModalNull() {
    this.cModal = "";
  }

  public updatePasswordForm(formData) {
    if (formData) {
      //this.spinner.show()
      this.api
        .post("/api/v1/seller/update-password", {
          email: formData["femail"],
          code: formData["code"],
          password: formData["password"]
        })
        .subscribe(
          (res: Response) => {
            console.log("res");
            console.log(res);
            //this.spinner.hide();
            this.cModal = "";
            this.onCloseHandled();
            if (res["status"].toString() == "success") {
              this.toast.success(res["message"]);
            } else {
              this.toast.error("Some Problem Occured");
            }
          },
          (err: Response) => {
            this.cModal = "";
            this.onCloseHandled();
            console.log("err");
            console.log(err);
            //this.spinner.hide();
            this.toast.error("Something went wrong");

            if (err["error"].message == "Errors") {
              this.toast.error(err["error"].data.errors[0].msg);
            } else {
              this.toast.error(err["error"].message);
            }
          }
        );
    }
  }

  public confirmAccount(confirmCode) {
    this.api.get("/api/v1/seller/confirm-account/" + confirmCode).subscribe(
      (res: Response) => {
        if (res["status"].toString() == "success") {
          this.toast.success("Now You can Log In");
          this.toast.success("Account Confirmed");
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
      }
    );
  }

  public saveLoginForm(formData) {
    if (formData) {
      //this.spinner.show();
      this.api
        .post("/api/v1/seller/login", {
          email: formData["lemail"],
          password: formData["lpassword"]
        })
        .subscribe(
          (res: Response) => {
            //this.spinner.hide();
            console.log(res);
            if (res["status"].toString() == "success") {
              //this.cookies.set("userId", res["data"]["uid"]);
              //this.cookies.set("name", res["data"]["name"]);
              this.cookies.set("orianaSellerToken", res["data"]["token"]);
              this.cookies.set("orianaSellerId", res["data"]["_id"]);
              this.cookies.set("orianaSellerEmail", res["data"]["email"]);
              //this.cookies.set("role", "seller");
              //this.apis.loggedIn = this.cookies.get("name");

              this.toast.success(res["message"]);

              this.router.navigate(["/dashboard"]);
            } else {
              this.toast.error("Some Problem Occured");
            }
          },
          (res: Response) => {
            //this.spinner.hide();
            if (res["error"].message == "Errors") {
              this.toast.error(res["error"].data.errors[0].msg);
            } else {
              this.toast.error(res["error"].message);
            }
          }
        );
    }
  }
}
