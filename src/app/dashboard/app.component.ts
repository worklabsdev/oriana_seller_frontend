import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class DashboardComponent {
  title = "app";
  isLogin = true;

  constructor(
    private api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private router: Router
  ) {}

  // changeOfRoutes(){
  //     var token =  this.cookies.get('token')
  // 	if(token){
  // 		this.api.post('/seller/auth',{}).subscribe((res: Response) => {
  // 		}, (res: Response) => {
  // 			this.router.navigate(['./login'])
  // 		})
  // 	}
  // 	else
  // 	{
  // 		this.router.navigate(['./login'])
  // 	}
  // }
}
