import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ApiService } from "../../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";
import { NavService } from "../nav-service.service";
declare var jQuery: any;
declare var $: any;
import "rxjs";
@Component({
  selector: "app-orders",
  templateUrl: "./orders.component.html",
  styleUrls: ["./orders.component.css"]
})
export class OrdersComponent implements OnInit {
  public allOrders = [];
  public currOrderId: any = "";
  public order = [];
  public list: any = "";
  public status: any = "";
  public nonCompOrders = [];
  public compOrders = [];
  public cancelOrders = [];
  constructor(
    private api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private nav: NavService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.nav.setPath({
      path: "overview",
      breadcrumbs: [
        { name: "Dashboard", path: "/dashboard" },
        { name: "Orders" }
      ]
    });
    window.scrollTo(0, 0);
    this.getOrders();
  }

  selectStatus(checkVal) {
    this.status = checkVal;
  }

  public openDeleteModel(content, id) {
    this.currOrderId = id;
    console.log("currOrderId");
    console.log(this.currOrderId);
    $("#orderstatus_modal").modal({ show: true });
    $("#orderstatus_modal").appendTo("body");
  }

  public getOrders() {
    this.allOrders = [];
    this.api.get("/api/v1/seller/orders").subscribe(
      (res: Response) => {
        console.log(res);
        if (res["status"].toString() == "success") {
          this.allOrders = res["data"];
          if (this.allOrders.length != 0) {
            this.list = 1;
          } else {
            this.list = "";
          }

          Array.prototype.forEach.call(this.allOrders, element => {
            if (element.status == "Pending") {
              this.nonCompOrders = [];
              this.nonCompOrders.push(element);
            } else if (element.status == "Completed") {
              this.compOrders = [];
              this.compOrders.push(element);
            } else if (element.status == "Cancelled") {
              this.cancelOrders = [];
              this.cancelOrders.push(element);
            }
          });
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
      }
    );
  }

  // public getOrderDetails(id) {
  //   this.order = [];
  //   this.api.get("/api/v1/seller/order" + id).subscribe(
  //     (res: Response) => {
  //       console.log(res);
  //       if (res["status"].toString() == "success") {
  //         this.order = res["data"];
  //       } else {
  //         this.toast.error("Some Problem Occured");
  //       }
  //     },
  //     (res: Response) => {
  //       console.log(res);
  //     }
  //   );
  // }

  public changeOrderStatus() {
    this.api
      .post("/api/v1/seller/change-order-status", {
        id: this.currOrderId,
        status: this.status
      })
      .subscribe(
        (res: Response) => {
          console.log(res);
          if (res["status"].toString() == "success") {
            this.getOrders();
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (res: Response) => {
          console.log(res);
        }
      );
  }
}
