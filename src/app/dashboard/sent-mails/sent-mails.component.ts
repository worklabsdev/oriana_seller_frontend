import { Component, OnInit } from '@angular/core';
import { NavService } from '../nav-service.service';

@Component({
  selector: 'app-sent-mails',
  templateUrl: './sent-mails.component.html',
  styleUrls: ['./sent-mails.component.css']
})
export class SentMailsComponent implements OnInit {

  constructor( private nav:NavService ) { }

  ngOnInit() {
    this.nav.setPath({'path':'messages', breadcrumbs:[
      {name:'Dashboard', path:'/dashboard'},
      {name:'Sent Messages'} 
    ]})
		window.scrollTo(0,0);
  }

}
