import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "../../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";
import { NavService } from "../nav-service.service";
import { ImageCroppedEvent } from "ngx-image-cropper/src/image-cropper.component";

@Component({
  selector: "app-add-product",
  templateUrl: "./add-product.component.html",
  styleUrls: ["./add-product.component.css"]
})
export class AddProductComponent implements OnInit {
  product: any = {};
  sizes = [];
  file: any;
  services: any = ["Cash on delivery is availeble"];
  colors = [];
  categories: any = [];
  public subCategories: any = [];
  public color: any;
  public category: any = "";
  imageChangedEvent: any = "";
  croppedImage: any = "";

  constructor(
    private api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private nav: NavService,
    private router: Router
  ) {}

  ngOnInit() {
    this.nav.setPath({
      path: "products",
      breadcrumbs: [
        { name: "Dashboard", path: "/dashboard" },
        { name: "Products", path: "/dashboard/products" },
        { name: "Add product" }
      ]
    });
    window.scrollTo(0, 0);
    this.getAllCategories();
  }

  private getAllCategories() {
    console.log("getAllCategories");
    this.api.get("/api/v1/seller/getCategory").subscribe(
      (res: Response) => {
        console.log("category", res);
        if (res["status"].toString() == "success") {
          this.categories = res["data"];
        } else {
          this.categories = [];
          this.toast.error("Some Problem Occured");
        }
      },
      (err: Response) => {
        console.log(err);
        if (err["error"].message == "Errors") {
          this.toast.error(err["error"].data.errors[0].msg);
        } else {
          this.toast.error(err["error"].message);
        }
      }
    );
  }

  selectSubCategory(checkVal) {
    console.log("something", checkVal);
    this.api.get("/api/v1/seller/getsubcategory/" + checkVal).subscribe(
      (res: Response) => {
        console.log(res);
        if (res["status"].toString() == "success") {
          this.subCategories = res["data"][0].split(",");
          console.log("this.subCategories", this.subCategories);
        } else {
          this.subCategories = [];
          this.toast.error("Some Problem Occured");
        }
      },
      (err: Response) => {
        console.log(err);
        // if (err["error"].message == "Errors") {
        //   console.log(err["error"].data.errors[0].msg);
        // } else {
        //   console.log(err["error"].message);
        // }
      }
    );

    for (var i = 0; i < this.categories.length; i++) {
      if (this.categories[i]._id == checkVal) {
        this.category = this.categories[i].name;
      }
      console.log(this.category);
    }
    //console.log("category", this.product.category);
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    console.log(event);
    this.croppedImage = event.base64;
    console.log(this.croppedImage);
    console.log(this.product.image);
    var blob = new Blob([this.croppedImage], { type: "image/png" });
    this.file = new File([blob], "imageFileName.png");
    // var product_data = new FormData();
    // // file ={fieldname: 'productPic',
    // // originalname: 'stock list_2.png',
    // // encoding: '7bit',
    // // mimetype: 'image/png',
    // // destination: './uploads',
    // // filename: '1543915635033-stock list_2.png',
    // // path: 'uploads/1543915635033-stock list_2.png',
    // // size: 238079}
    // product_data.append("uploads", file);
    // this.product["productPic"] = product_data;
    console.log("file");
    // console.log(file);
    this.file.base64 = this.croppedImage;
    console.log(typeof this.file);
  }

  imageLoaded() {
    // show cropper
  }
  loadImageFailed() {
    this.toast.error("Image Load Failed");
  }

  /* updateImage(files: any) {
    this.product["productPic"] = files.item(0);
  } */

  selectSize(checkVal) {
    var valIndex = this.sizes.indexOf(checkVal);
    if (valIndex == -1) {
      this.sizes.push(checkVal);
    } else {
      this.sizes.splice(valIndex, 1);
    }
  }

  selectColor(checkVal) {
    var valIndex = this.colors.indexOf(checkVal);
    if (valIndex == -1) {
      this.colors.push(checkVal);
    } else {
      this.colors.splice(valIndex, 1);
    }
  }

  selectServices(checkVal) {
    var valIndex = this.services.indexOf(checkVal);
    if (valIndex == -1) {
      this.services.push(checkVal);
    } else {
      this.services.splice(valIndex, 1);
    }
  }

  addProduct() {
    var product_data = new FormData();
    product_data.append("productPic", this.file);
    product_data.append("name", this.product.name);
    product_data.append("aprice", this.product.aprice);
    product_data.append("dprice", this.product.dprice);
    product_data.append("description", this.product.description);
    product_data.append("category", this.category);
    product_data.append("subcategory", this.product.subcategory);
    product_data.append("quantity", this.product.quantity);
    product_data.append("quantityType", this.product.quantityType);
    product_data.append("gender", this.product.gender);
    product_data.append("sellerEmail", this.cookies.get("orianaSellerEmail"));
    product_data.append("sellerId", this.cookies.get("orianaSellerId"));
    for (var i = 0; i < this.sizes.length; i++) {
      product_data.append("size", this.sizes[i]);
    }
    for (var j = 0; j < this.colors.length; j++) {
      product_data.append("color", this.colors[j]);
    }
    for (var k = 0; k < this.services.length; k++) {
      product_data.append("service", this.services[k]);
    }
    product_data.append("productPicbase64", this.croppedImage);

    this.api
      .post(
        "/api/v1/seller/product",
        product_data
        //  {
        //   name: this.product.name,
        //   aprice: this.product.aprice,
        //   dprice: this.product.dprice,
        //   description: this.product.description,
        //   size: this.sizes,
        //   service: this.services,
        //   color: this.colors,
        //   category: this.category,
        //   subcategory: this.product.subcategory,
        //   quantity: this.product.quantity,
        //   quantityType: this.product.quantityType,
        //   gender: this.product.gender,
        //   productPic: this.product.productPic
        // }
      )
      .subscribe(
        (res: Response) => {
          if (res["status"].toString() == "success") {
            this.toast.success("Product Added Succesfully");
            this.router.navigate(["/dashboard/products"]);
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (res: Response) => {
          if (res["error"].message == "Errors") {
            this.toast.error(res["error"].data.errors[0].msg);
          } else {
            this.toast.error(res["error"].message);
          }
        }
      );
  }
}
