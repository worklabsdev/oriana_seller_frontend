import { Component, OnInit } from "@angular/core";
import { NavService } from "../nav-service.service";
import { Router } from "@angular/router";
import { ApiService } from "../../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";
@Component({
  selector: "app-account-information",
  templateUrl: "./account-information.component.html",
  styleUrls: ["./account-information.component.css"]
})
export class AccountInformationComponent implements OnInit {
  seller: any = {};
  profilePicBtn: Boolean = false;
  profilePic: any = "";

  constructor(
    private api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private nav: NavService,
    private router: Router
  ) {}

  ngOnInit() {
    this.nav.setPath({
      path: "account-information",
      breadcrumbs: [
        { name: "Dashboard", path: "/dashboard" },
        { name: "Account Information" }
      ]
    });
    window.scrollTo(0, 0);
    this.getDetails();
  }

  public getDetails() {
    this.api.get("/api/v1/seller/details").subscribe(
      (res: Response) => {
        console.log(res);
        console.log("vikki");
        if (res["status"].toString() == "success") {
          console.log(res["data"]);
          //	res['data'].image ? res['data'].image =  this.api.serverurl + res['data'].image : res['data'].image = 'assets/img/admin.png'

          this.seller = res["data"];
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
        if (res["error"].message == "Errors") {
          this.toast.error(res["error"].data.errors[0].msg);
        } else {
          this.toast.error(res["error"].message);
        }
      }
    );
  }

  doUpdateProfile() {
    this.api.put("/api/v1/seller/update-account", this.seller).subscribe(
      (res: Response) => {
        if (res["status"].toString() == "success") {
          this.toast.success("Seller Successfully Updated");
          this.router.navigate(["/dashboard"]);
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        if (res["error"].message == "Errors") {
          this.toast.error(res["error"].data.errors[0].msg);
        } else {
          this.toast.error(res["error"].message);
        }
      }
    );
  }

  updateProfileBtn(files: any) {
    //this.profilePicBtn = true;
    //this.profilePic = files.item(0);
    const formData: FormData = new FormData();
    formData.append("avtar", files.item(0));
    console.log(files.item(0));
    this.api.put("/api/v1/seller/profile-image", formData).subscribe(
      (res: Response) => {
        if (res["status"].toString() == "success") {
          //this.seller.image = res["data"].image;
          this.seller.image = res["data"]["url"];
          //this.getDetails();
          this.toast.success("Profile Pic Successfully Updated");
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        if (res["error"].message == "Errors") {
          this.toast.error(res["error"].data.errors[0].msg);
        } else {
          this.toast.error(res["error"].message);
        }
      }
    );
  }

  removeProfileImage() {
    this.profilePic = "";
    const formData: FormData = new FormData();
    formData.append("avtar", this.profilePic);
    this.api.delete("/api/v1/seller/delete-profile-image").subscribe(
      (res: Response) => {
        if (res["status"].toString() == "success") {
          //this.seller.image = res["data"].image;
          this.seller.image = "";
          this.toast.success("Profile Pic Removed Successfully");
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        if (res["error"].message == "Errors") {
          this.toast.error(res["error"].data.errors[0].msg);
        } else {
          this.toast.error(res["error"].message);
        }
      }
    );
  }
}
