import { Component, OnInit } from '@angular/core';
import { NavService } from '../nav-service.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  constructor( private nav:NavService) { }

  ngOnInit() {
    this.nav.setPath({'path':'messages', breadcrumbs:[
      {name:'Dashboard', path:'/dashboard'},
      {name:'Inbox'} 
    ]})
		window.scrollTo(0,0);
  }

}
