import { Component, OnInit } from "@angular/core";
import { NavService } from "../nav-service.service";
import { Router } from "@angular/router";
import { ApiService } from "../../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";

@Component({
  selector: "app-shop",
  templateUrl: "./shop.component.html",
  styleUrls: ["./shop.component.css"]
})
export class ShopComponent implements OnInit {
  public allShops = [];
  public shop: any = {};
  profilePicBtn: Boolean = false;
  profilePic: any = "";
  list: any = "";
  documents: any = "";

  constructor(
    public apis: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private nav: NavService,
    private router: Router
  ) {}

  ngOnInit() {
    this.nav.setPath({
      path: "shop",
      breadcrumbs: [{ name: "Dashboard", path: "/dashboard" }, { name: "Shop" }]
    });
    window.scrollTo(0, 0);
    this.getAllShops();
  }

  getAllShops() {
    this.apis
      .post("/api/v1/seller/shops", {
        seller_id: this.cookies.get("orianaSellerId")
      })
      .subscribe(
        (res: Response) => {
          console.log(res);

          if (res["status"].toString() == "success") {
            this.allShops = res["data"];
            if (this.allShops.length != 0) {
              this.list = 1;
              if (this.allShops[0]["shopManagementPic"].length != 0) {
                this.documents = 1;
              } else {
                this.documents = "";
              }
            } else {
              this.list = "";
            }

            console.log("allShops", this.allShops);
          }
          // else {
          //   this.toast.error("Some Problem Occured");
          // }
        },
        (error: any) => {
          console.log("this is the error ", error);
        }
      );
  }

  deleteShop(id) {
    this.apis.delete("/api/v1/seller/shop/" + id).subscribe(
      (res: Response) => {
        console.log(res);
        if (res["status"].toString() == "success") {
          this.getAllShops();
          this.toast.success("Deleted Successfully");
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (error: any) => {
        console.log("this is the error ", error);
      }
    );
  }

  // getShopDetails(id) {
  //   this.apis.get("/api/v1/seller/shop/" + id).subscribe(
  //     (res: Response) => {
  //       console.log("buyerInfo");
  //       console.log(res);
  //       if (res["status"].toString() == "success") {
  //         this.shop = res["data"];
  //         console.log(this.shop);
  //       } else {
  //         this.toast.error("Some Problem Occured");
  //       }
  //     },
  //     (err: Response) => {
  //       console.log(err);
  //       if (err["error"].message == "Errors") {
  //         this.toast.error(err["error"].data.errors[0].msg);
  //       } else {
  //         this.toast.error(err["error"].message);
  //       }
  //     }
  //   );
  // }

  // updateBuyerDetails(id) {
  //   this.apis
  //     .post("/api/v1/admin/user/update", {
  //       token: this.cookies.get("orianaSellerToken"),
  //       _id: this.buyer["_id"],
  //       name: this.buyer["name"],
  //       email: this.buyer["email"],
  //       phone: this.buyer["phone"]
  //     })
  //     .subscribe(
  //       (res: Response) => {
  //         console.log("buyerUpdate");
  //         console.log(res);
  //         if (res["status"].toString() == "success") {
  //           this.getAllBuyers();
  //         } else {
  //           this.toast.error("Some Problem Occured");
  //         }
  //       },
  //       (err: Response) => {
  //         console.log(err);
  //         if (err["error"].message == "Errors") {
  //           this.toast.error(err["error"].data.errors[0].msg);
  //         } else {
  //           this.toast.error(err["error"].message);
  //         }
  //       }
  //     );
  // }

  // addShop() {
  //   this.apis
  //     .post("/api/v1/seller/addShop", {
  //       name: this.shop["name"],
  //       email: this.shop["email"],
  //       description: this.shop["description"],
  //       banner: this.shop["banner"],
  //       status: "ON",
  //       address: this.shop["address"],
  //       country: this.shop["country"],
  //       state: this.shop["state"],
  //       city: this.shop["city"],
  //       postal_code: this.shop["postal_code"],
  //       license_no: this.shop["license_no"],
  //       gst_no: this.shop["gst_no"],
  //       pancard_no: this.shop["pancard_no"],
  //       contact_no: this.shop["contact_no"],
  //       shopkeeper_name: this.shop["shopkeeper_name"],
  //       shopkeeper_no: this.shop["shopkeeper_no"],
  //       open_time: this.shop["open_time"],
  //       close_time: this.shop["close_time"],
  //       shopLogo: this.shop["shopLogo"],
  //       gstCertificate: this.shop["gstCertificate"],
  //       cancelCheck: this.shop["cancelCheck"],
  //       PanCard: this.shop["PanCard"],
  //       shopCertificate: this.shop["shopCertificate"]
  //     })
  //     .subscribe(
  //       (res: Response) => {
  //         console.log("buyerInfo");
  //         console.log(res);
  //         if (res["status"].toString() == "success") {
  //           this.getAllShops();
  //         } else {
  //           this.toast.error("Some Problem Occured");
  //         }
  //       },
  //       (err: Response) => {
  //         console.log(err);
  //         if (err["error"].message == "Errors") {
  //           this.toast.error(err["error"].data.errors[0].msg);
  //         } else {
  //           this.toast.error(err["error"].message);
  //         }
  //       }
  //     );
  // }
}
