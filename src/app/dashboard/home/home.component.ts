import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "../../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";
import { NavService } from "../nav-service.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  totalProd: number = 0;
  ordersLength: number = 0;
  currProduct: any = {};
  latestOrders = [];
  latest: any = "";
  constructor(
    private api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private nav: NavService,
    private router: Router
  ) {}

  ngOnInit() {
    this.nav.setPath({
      path: "overview",
      breadcrumbs: [{ name: "Dashboard" }]
    });
    window.scrollTo(0, 0);
    this.getProdQuantity();
    this.getOrdersQty();
  }

  public getProdQuantity() {
    this.api.get("/api/v1/seller/products").subscribe(
      (res: Response) => {
        if (res["status"].toString() == "success") {
          for (var i = 0; i < res["data"].length; i++) {
            this.totalProd += res["data"][i].quantity;
          }
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
        if (res["error"].message == "Errors") {
          this.toast.error(res["error"].data.errors[0].msg);
        } else {
          this.toast.error(res["error"].message);
        }
      }
    );
  }

  public getOrdersQty() {
    this.api.get("/api/v1/seller/orders").subscribe(
      (res: Response) => {
        console.log(res);
        if (res["status"].toString() == "success") {
          this.ordersLength = res["data"].length;
          if (this.ordersLength > 0) {
            this.latest = 1;
          }
          for (var i = 0; i < res["data"].length; i++) {
            if (i < 5) {
              this.latestOrders.push(res["data"][i]);
            }
          }
        }
      },
      (res: Response) => {
        console.log(res);
      }
    );
  }
}
