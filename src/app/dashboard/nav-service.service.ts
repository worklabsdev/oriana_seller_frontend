import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable()
export class NavService {
  constructor() {}

  public currPath = new Subject<string>();

  setPath(value: any) {
    this.currPath.next(value);
  }
}
