import { Component, OnInit } from "@angular/core";
import { NavService } from "../nav-service.service";
import { Router, ActivatedRoute } from "@angular/router";
import { ApiService } from "../../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";
@Component({
  selector: "app-view-shop",
  templateUrl: "./view-shop.component.html",
  styleUrls: ["./view-shop.component.css"]
})
export class ViewShopComponent implements OnInit {
  public shop: any = {};
  profilePicBtn: Boolean = false;
  profilePic: any = "";

  constructor(
    public apis: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private nav: NavService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.nav.setPath({
      path: "shop",
      breadcrumbs: [
        { name: "Dashboard", path: "/dashboard" },
        { name: "View Shop Details" }
      ]
    });
    window.scrollTo(0, 0);

    this.route.params.subscribe(params => {
      var id = params["id"];
      this.getShopDetails(id);
    });
  }
  getShopDetails(id) {
    this.apis.get("/api/v1/seller/shop" + id).subscribe(
      (res: Response) => {
        console.log(res);
        if (res["status"].toString() == "success") {
          this.shop = res["data"];
          console.log(this.shop);
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (err: Response) => {
        console.log(err);
        if (err["error"].message == "Errors") {
          this.toast.error(err["error"].data.errors[0].msg);
        } else {
          this.toast.error(err["error"].message);
        }
      }
    );
  }
}
