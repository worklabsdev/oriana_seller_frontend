import { Component, OnInit } from "@angular/core";
import { NavService } from "../nav-service.service";
import { Router } from "@angular/router";
import { ApiService } from "../../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";

@Component({
  selector: "app-add-shop",
  templateUrl: "./add-shop.component.html",
  styleUrls: ["./add-shop.component.css"]
})
export class AddShopComponent implements OnInit {
  public doc_64textString: any = "";
  public doc_64textString1: any = "";
  public doc_64textString2: any = "";
  public doc_64textString3: any = "";
  public doc_64textString4: any = "";
  public allShops = [];
  public shopManagementPic = [];
  public shop: any = {};
  profilePicBtn: Boolean = false;
  profilePic: any = "";

  constructor(
    public apis: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private nav: NavService,
    private router: Router
  ) {}

  ngOnInit() {
    this.nav.setPath({
      path: "shop",
      breadcrumbs: [
        { name: "Dashboard", path: "/dashboard" },
        { name: "Add Shop Details" }
      ]
    });
    window.scrollTo(0, 0);
  }

  logo(files) {
    var file = files[0];
    if (files && file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
    this.shopManagementPic.push(files.item(0));
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.doc_64textString = "data:image/png;base64," + btoa(binaryString);
  }

  gst(files) {
    var file = files[0];
    if (files && file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded1.bind(this);
      reader.readAsBinaryString(file);
    }
    this.shopManagementPic.push(files.item(0));
  }

  _handleReaderLoaded1(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.doc_64textString1 = "data:image/png;base64," + btoa(binaryString);
  }

  check(files) {
    var file = files[0];
    if (files && file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded2.bind(this);
      reader.readAsBinaryString(file);
    }
    this.shopManagementPic.push(files.item(0));
  }

  _handleReaderLoaded2(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.doc_64textString2 = "data:image/png;base64," + btoa(binaryString);
  }

  pancard(files) {
    var file = files[0];
    if (files && file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded3.bind(this);
      reader.readAsBinaryString(file);
    }
    this.shopManagementPic.push(files.item(0));
  }

  _handleReaderLoaded3(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.doc_64textString3 = "data:image/png;base64," + btoa(binaryString);
  }

  certificate(files) {
    var file = files[0];
    if (files && file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded4.bind(this);
      reader.readAsBinaryString(file);
    }
    this.shopManagementPic.push(files.item(0));
  }

  _handleReaderLoaded4(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.doc_64textString4 = "data:image/png;base64," + btoa(binaryString);
  }

  selectShopStatus(checkVal) {
    var valIndex = this.shop["status"].indexOf(checkVal);
    if (valIndex == -1) {
      this.shop["status"] = checkVal;
    }
  }

  public addNewShop() {
    const formData: FormData = new FormData();
    formData.append("name", this.shop["name"]);
    formData.append("email", this.shop["email"]);
    formData.append("description", this.shop["description"]);
    formData.append("banner", this.shop["banner"]);
    formData.append("status", this.shop["status"]);
    formData.append("address", this.shop["address"]);
    formData.append("country", this.shop["country"]);
    formData.append("state", this.shop["state"]);
    formData.append("city", this.shop["city"]);
    formData.append("postal_code", this.shop["postal_code"]);
    formData.append("license_no", this.shop["license_no"]);
    formData.append("gst_no", this.shop["gst_no"]);
    formData.append("pancard_no", this.shop["pancard_no"]);
    formData.append("contact_no", this.shop["contact_no"]);
    formData.append("shopkeeper_name", this.shop["shopkeeper_name"]);
    formData.append("shopkeeper_no", this.shop["shopkeeper_no"]);
    formData.append("open_time", this.shop["open_time"]);
    formData.append("close_time", this.shop["close_time"]);

    for (var i = 0; i < this.shopManagementPic.length; i++) {
      formData.append("shopManagementPic", this.shopManagementPic[i]);
    }
    console.log(this.shopManagementPic);
    this.apis
      .post(
        "/api/v1/seller/addShop",
        formData
        // name: this.shop["name"],
        // email: this.shop["email"],
        // description: this.shop["description"],
        // banner: this.shop["banner"],
        // status: "ON",
        // address: this.shop["address"],
        // country: this.shop["country"],
        // state: this.shop["state"],
        // city: this.shop["city"],
        // postal_code: this.shop["postal_code"],
        // license_no: this.shop["license_no"],
        // gst_no: this.shop["gst_no"],
        // pancard_no: this.shop["pancard_no"],
        // contact_no: this.shop["contact_no"],
        // shopkeeper_name: this.shop["shopkeeper_name"],
        // shopkeeper_no: this.shop["shopkeeper_no"],
        // open_time: this.shop["open_time"],
        // close_time: this.shop["close_time"],
        // shopManagementPic: formData
      )
      .subscribe(
        (res: Response) => {
          console.log("buyerInfo");
          console.log(res);
          if (res["status"].toString() == "success") {
            this.router.navigate(["/dashboard/shop"]);
            this.toast.success("Shop Added Successfully");
          } else {
            this.toast.error("Some Problem Occured");
          }
        },
        (err: Response) => {
          console.log(err);
          if (err["error"].message == "Errors") {
            this.toast.error(err["error"].data.errors[0].msg);
          } else {
            this.toast.error(err["error"].message);
          }
        }
      );
  }

  // public addNewShop() {
  //   this.apis
  //     .post("/api/v1/seller/addShop", {
  //       name: this.shop["name"],
  //       email: this.shop["email"],
  //       description: this.shop["description"],
  //       banner: this.shop["banner"],
  //       status: "ON",
  //       address: this.shop["address"],
  //       country: this.shop["country"],
  //       state: this.shop["state"],
  //       city: this.shop["city"],
  //       postal_code: this.shop["postal_code"],
  //       license_no: this.shop["license_no"],
  //       gst_no: this.shop["gst_no"],
  //       pancard_no: this.shop["pancard_no"],
  //       contact_no: this.shop["contact_no"],
  //       shopkeeper_name: this.shop["shopkeeper_name"],
  //       shopkeeper_no: this.shop["shopkeeper_no"],
  //       open_time: this.shop["open_time"],
  //       close_time: this.shop["close_time"],
  //       shopManagementPic: this.shopManagementPic
  //     })
  //     .subscribe(
  //       (res: Response) => {
  //         console.log("buyerInfo");
  //         console.log(res);
  //         if (res["status"].toString() == "success") {
  //           this.router.navigate(["/shop"]);
  //           this.toast.success("Shop Added Successfully");
  //         } else {
  //           this.toast.error("Some Problem Occured");
  //         }
  //       },
  //       (err: Response) => {
  //         console.log(err);
  //         if (err["error"].message == "Errors") {
  //           this.toast.error(err["error"].data.errors[0].msg);
  //         } else {
  //           this.toast.error(err["error"].message);
  //         }
  //       }
  //     );
  // }
}
