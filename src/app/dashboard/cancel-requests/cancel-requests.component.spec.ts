import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelRequestsComponent } from './cancel-requests.component';

describe('CancelRequestsComponent', () => {
  let component: CancelRequestsComponent;
  let fixture: ComponentFixture<CancelRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
