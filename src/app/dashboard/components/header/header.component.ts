import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";
import { ApiService } from "../../../services/api.service";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit {
  constructor(
    private api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private router: Router
  ) {}

  ngOnInit() {}

  public doLogout() {
    this.api.post("/api/v1/seller/logout", {}).subscribe(
      (res: Response) => {
        if (res["status"].toString() == "success") {
          this.cookies.delete("orianaSellerToken");
          this.cookies.delete("orianaSellerId");
          this.cookies.delete("orianaSellerEmail");
          this.toast.success("Successfully Logged Out");
          this.router.navigate(["/"]);
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
        if (res["error"].message == "Errors") {
          this.toast.error(res["error"].data.errors[0].msg);
        } else {
          this.toast.error(res["error"].message);
        }
      }
    );
  }
}
