import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { NavService } from "../../nav-service.service";
@Component({
  selector: "app-navigation",
  templateUrl: "./navigation.component.html",
  styleUrls: ["./navigation.component.css"]
})
export class NavigationComponent implements OnInit {
  public active: any;
  constructor(private route: ActivatedRoute, private nav: NavService) {}

  ngOnInit() {
    this.nav.currPath.subscribe(data => {
      this.active = data["path"];
      console.log(this.active);
    });
  }
}
