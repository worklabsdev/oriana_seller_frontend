import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ApiService } from "../../services/api.service";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";
import { NavService } from "../nav-service.service";
@Component({
  selector: "app-view-order",
  templateUrl: "./view-order.component.html",
  styleUrls: ["./view-order.component.css"]
})
export class ViewOrderComponent implements OnInit {
  public currProduct = [];
  public orderId: any = "";
  constructor(
    private api: ApiService,
    private toast: ToastrService,
    private cookies: CookieService,
    private nav: NavService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.nav.setPath({
      path: "overview",
      breadcrumbs: [
        { name: "Dashboard", path: "/dashboard" },
        { name: "Order" }
      ]
    });
    window.scrollTo(0, 0);
    this.route.params.subscribe(params => {
      var id = params["id"]; // (+) converts string 'id' to a number
      console.log(id);
      this.orderId = id;
      this.getOrder(id);
    });
  }

  public getOrder(id) {
    this.api.get("/api/v1/seller/order/" + id).subscribe(
      (res: Response) => {
        console.log(res);
        if (res["status"].toString() == "success") {
          this.currProduct = res["data"]["products"];
        } else {
          this.toast.error("Some Problem Occured");
        }
      },
      (res: Response) => {
        console.log(res);
        if (res["error"].message == "Errors") {
          this.toast.error(res["error"].data.errors[0].msg);
        } else {
          this.toast.error(res["error"].message);
        }
      }
    );
  }
}
