import { RouterModule, Routes } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { DashboardComponent } from "./dashboard/app.component";
import { HomeComponent } from "./dashboard/home/home.component";
import { ProductsComponent } from "./dashboard/products/products.component";
import { AddProductComponent } from "./dashboard/add-product/add-product.component";
import { ViewProductComponent } from "./dashboard/view-product/view-product.component";
import { EditProductComponent } from "./dashboard/edit-product/edit-product.component";
import { ChangePasswordComponent } from "./dashboard/change-password/change-password.component";
import { UpdateProfileComponent } from "./dashboard/update-profile/update-profile.component";
import { AccountInformationComponent } from "./dashboard/account-information/account-information.component";
import { AuthGuard } from "./guards/auth.guard";

import { ForgetPasswordComponent } from "./forget-password/forget-password.component";
import { BankAccountInformationComponent } from "./dashboard/bank-account-information/bank-account-information.component";
import { MessagesComponent } from "./dashboard/messages/messages.component";
import { SentMailsComponent } from "./dashboard/sent-mails/sent-mails.component";
import { TrashComponent } from "./dashboard/trash/trash.component";
import { ComposeComponent } from "./dashboard/compose/compose.component";
import { LoginConfirmComponent } from "./dashboard/login-confirm/login-confirm.component";
import { OrdersComponent } from "./dashboard/orders/orders.component";
import { SoldComponent } from "./dashboard/sold/sold.component";
import { ShopComponent } from "./dashboard/shop/shop.component";
import { AddShopComponent } from "./dashboard/add-shop/add-shop.component";
import { EditShopComponent } from "./dashboard/edit-shop/edit-shop.component";
import { ViewShopComponent } from "./dashboard/view-shop/view-shop.component";
import { CancelRequestsComponent } from "./dashboard/cancel-requests/cancel-requests.component";
import { ViewOrderComponent } from "./dashboard/view-order/view-order.component";

export const routes: Routes = [
  { path: "", component: LoginComponent },
  { path: "login", component: LoginComponent },
  { path: "forget-password", component: ForgetPasswordComponent },
  /* { 'path': 'login/:confirmCode', component: LoginConfirmComponent }, */
  { path: "login/:confirmCode", component: LoginConfirmComponent },

  {
    path: "dashboard",
    component: DashboardComponent,
    children: [
      { path: "", component: HomeComponent },
      { path: "messages", component: MessagesComponent },
      { path: "sent-mails", component: SentMailsComponent },
      { path: "trash", component: TrashComponent },
      { path: "compose", component: ComposeComponent },
      { path: "change-password", component: ChangePasswordComponent },
      { path: "products", component: ProductsComponent },
      { path: "add-product", component: AddProductComponent },
      { path: "view-product/:id", component: ViewProductComponent },
      { path: "edit-product/:id", component: EditProductComponent },
      { path: "account-information", component: AccountInformationComponent },
      {
        path: "bank-account-information",
        component: BankAccountInformationComponent
      },
      {
        path: "orders",
        component: OrdersComponent
      },
      {
        path: "order/:id",
        component: ViewOrderComponent
      },
      {
        path: "sold",
        component: SoldComponent
      },
      { path: "shop", component: ShopComponent },
      { path: "add-shop", component: AddShopComponent },
      { path: "view-shop/:id", component: ViewShopComponent },
      { path: "edit-shop/:id", component: EditShopComponent },
      { path: "cancel-requests", component: CancelRequestsComponent },

      { path: "update-profile", component: UpdateProfileComponent }
    ]
  }
];
